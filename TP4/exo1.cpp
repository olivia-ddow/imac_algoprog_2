#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    return nodeIndex * 2 + 1;
    //return 0;
}

int Heap::rightChild(int nodeIndex)
{
    return nodeIndex * 2 + 2;
    //return 0;
}

void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i = heapSize;
    (*this)[i] = value;
    while (i > 0 && (*this)[i] > (*this)[(i-1)/2]){
        swap(i, (i-1)/2);
        i = (i-1)/2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex)
{
	// use (*this)[i] or this->get(i) to get a value at index i
    int i_max = nodeIndex;
    int largest = i_max;

    int i_leftchild = this->leftChild(nodeIndex);
    int i_rightchild = this->rightChild(nodeIndex);

    if (i_leftchild < heapSize && this->get(i_leftchild) > (*this)[largest]){
       largest = i_leftchild;
    }
    if (i_rightchild < heapSize && this->get(i_rightchild) > (*this)[largest]){
        largest = i_rightchild;
    }
    if (largest != i_max){
        swap(largest, i_max);
        heapify(heapSize, largest);
    }
}

void Heap::buildHeap(Array& numbers)
{
    //for (int i : numbers){
    for (int i=0; i < numbers.size(); i++){
        this->insertHeapNode(this->size(),i);
    }

}

void Heap::heapSort()
{
    for(int i = (this->size()-1); i>=0; i--) {
            this->swap(0,i);
            this->heapify(i,0);
    }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
    w->show();

    return a.exec();
}
