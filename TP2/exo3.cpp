#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;


void bubbleSort(Array& toSort){
	// bubbleSort
    bool est_permute;
    int passe=0;
    do {
        est_permute=false;
        passe++;
        for(uint i=0; i<toSort.size()-passe;i++){
            if (toSort[i]>toSort[i+1]){
                toSort.swap(i, i+1);
                est_permute=true;
            }
        }
    }while(est_permute);
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 100;
	w = new TestMainWindow(bubbleSort);
	w->show();

	return a.exec();
}
