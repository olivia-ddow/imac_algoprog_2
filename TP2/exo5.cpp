#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)

	// initialisation
	Array& first = w->newArray(origin.size()/2);
	Array& second = w->newArray(origin.size()-first.size());
	
    // split
    for (uint i=0; i < first.size(); i++){
            first[i] = origin[i];
    }
    int index_s = 0;
    for (uint j = first.size(); j<origin.size(); j++){
            second[index_s] = origin[j];
            index_s++;

    }
	// recursiv splitAndMerge of lowerArray and greaterArray
    if (first.size() > 1){
        splitAndMerge(first);
    }
    if (second.size() > 1){
        splitAndMerge(second);
    }
	// merge
    merge(first, second, origin);
}

void merge(Array& first, Array& second, Array& result)
{
    uint iF_encours=0, iS_encours=0;

    for (uint iR_encours = 0; iR_encours < result.size(); iR_encours++){
        if (iS_encours < second.size() && iF_encours < first.size()){
            if(second[iS_encours] < first[iF_encours]){
                result[iR_encours] = second[iS_encours];
                iS_encours++;
            }else{
                result[iR_encours] = first[iF_encours];
                iF_encours++;
            }
        }else if (iS_encours >= second.size()){
            result[iR_encours] = first[iF_encours];
            iF_encours++;
        }else{
            result[iR_encours] = second[iS_encours];
            iS_encours++;
        }
    }
}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
