#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;

void insertionSort(Array& toSort){
	Array& sorted=w->newArray(toSort.size());
    // insertion sort from toSort to sorted
    sorted[0]=toSort[0];
    int pos=1;
    for(uint j=1; j<toSort.size(); j++){
       int m=0;
       for(;m<pos; m++){
           if(toSort[j]<sorted[m]){
               sorted.insert(m,toSort[j]);
               //pos++;
               break;
           }
       }
       if(m==pos){
           sorted.insert(pos,toSort[j]);
           //pos++;
       }
       pos++;
    }
	toSort=sorted; // update the original array
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(insertionSort); // window which display the behavior of the sort algorithm
	w->show();

	return a.exec();
}
