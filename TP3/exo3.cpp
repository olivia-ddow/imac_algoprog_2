#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct SearchTreeNode : public Node
{    
    SearchTreeNode* left;
    SearchTreeNode* right;
    int value;

    void initNode(int value)
    {
        // init initial node without children
        this->left = nullptr;
        this->right = nullptr;
        this->value = value;
    }

	void insertNumber(int value) {
        // create a new node and insert it in right or left child
        if (value < this->value){
            if (this->left == nullptr){
                this->left = new SearchTreeNode(value);
            }else{
                this->left->insertNumber(value);
            }
        } else {
            if (this->right == nullptr){
                this->right = new SearchTreeNode(value);
            }else {
                this->right->insertNumber(value);
            }
        }
    }

	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        uint left_height = 0, right_height = 0;
        if (this->left != nullptr){
            left_height = this->left->height();
        }
        if (this->right != nullptr){
            right_height = this->right->height();
        }
        if (left_height >= right_height){
            return left_height + 1;
        } else {
            return right_height + 1;
        }
        //return 1;
    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        uint left_cpt = 0, right_cpt = 0;
        if (this->left != nullptr){
            left_cpt = this->left->nodesCount();
        }
        if (this->right != nullptr){
            right_cpt = this->right->nodesCount();
        }
        return left_cpt + right_cpt + 1;
        //return 1;
	}

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)
        if (this->left == nullptr && this->right == nullptr){
            return true;
        }
        return false;
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
        if (this->isLeaf()){
            leaves[leavesCount] = this;
            leavesCount++;
        }
        if (this->left != nullptr){
            this->left->allLeaves(leaves, leavesCount);
        }
        if (this->right != nullptr){
            this->right->allLeaves(leaves, leavesCount);
        }
	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
        if (this->left != nullptr){
            this->left->inorderTravel(nodes, nodesCount);
        }
        nodes[nodesCount] = this;
        nodesCount++;
        if (this->right != nullptr){
            this->right->inorderTravel(nodes, nodesCount);
        }
	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
        nodes[nodesCount] = this;
        nodesCount++;
        if (this->left != nullptr){
            this->left->preorderTravel(nodes, nodesCount);
        }
        if (this->right != nullptr){
            this->right->preorderTravel(nodes, nodesCount);
        }
	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
        if (this->left != nullptr){
            this->left->postorderTravel(nodes, nodesCount);
        }
        if (this->right != nullptr){
            this->right->postorderTravel(nodes, nodesCount);
        }
        nodes[nodesCount] = this;
        nodesCount++;
	}

	Node* find(int value) {
        Node* val_left = nullptr;
        Node* val_right = nullptr;
        // find the node containing value
        if (this->value == value){
            return this;
        }
        if (this->left != nullptr){
            val_left = this->left->find(value);
        }
        if (val_left == nullptr && this->right != nullptr){
            val_right = this->right->find(value);
        }
        if (val_left != nullptr){
            return val_left;
        }
        if (val_right != nullptr){
            return val_right;
        }
        return nullptr;
	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    SearchTreeNode(int value) : Node(value) {initNode(value);}
    ~SearchTreeNode() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new SearchTreeNode(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
	w->show();

	return a.exec();
}
