#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    // your code
    Noeud* dernier;
};

struct DynaTableau{
    int* donnees;
    // your code
    int capacite;
    int nbValeurs;
};


void initialise(Liste* liste)
{
    liste->premier = nullptr;
    liste->dernier = nullptr;

}

bool est_vide(const Liste* liste)
{
    if (liste->premier==nullptr){
        return true;
    }
    return false;
}

void ajoute(Liste* liste, int valeur)
{
    Noeud* nv_noeud = new Noeud;
    nv_noeud->donnee = valeur;
    nv_noeud->suivant = nullptr;
   if (!est_vide(liste)){
        liste->dernier->suivant = nv_noeud;
        liste->dernier = nv_noeud;
    }else{
       liste->premier = nv_noeud;
       liste->dernier= nv_noeud;
       //liste->premier->suivant = liste->dernier;
    }

}

void affiche(const Liste* liste)
{
    int cpt=0;
    if (est_vide(liste)){
        std::cout<<"Not enough memory!"<<std::endl;
    }
    Noeud *noeud_temp = liste->premier;
    while (noeud_temp!=NULL){
        std::cout << "Le noeud " << cpt << " : "<< noeud_temp->donnee << std::endl;
        noeud_temp = noeud_temp->suivant;
        cpt++;
    }


}
int recupere(const Liste* liste, int n)
{
    if (est_vide(liste)){
        return 0;
    }
    Noeud *noeud_temp = liste->premier;
    for (int i=0; i<n; i++){
        noeud_temp = noeud_temp->suivant;
    }
    return noeud_temp->donnee;
}

int cherche(const Liste* liste, int valeur)
{
    int cpt=0;
    Noeud *noeud_temp = liste->premier;
    while (noeud_temp != NULL){
           if (noeud_temp->donnee==valeur){
               return cpt;
           }
           noeud_temp = noeud_temp->suivant;
           cpt++;
       }
    return -1;
}

void stocke(Liste* liste, int n, int valeur)
{
    Noeud *noeud_temp = liste->premier;
    for (int i=0; i<n; i++){
        noeud_temp = noeud_temp->suivant;
    }
    noeud_temp->donnee = valeur;

}

////////////FONCTIONS DYNATABLEAU/////////////////
void initialise(DynaTableau* tableau, int capacite)
{
    tableau->capacite = capacite;
    tableau->donnees = (int *) malloc(sizeof(int)*capacite);
    tableau->nbValeurs = 0;
}

void ajoute(DynaTableau* tableau, int valeur)
{
    if (tableau->capacite > tableau->nbValeurs){
        tableau->donnees[tableau->nbValeurs]=valeur;
        tableau->nbValeurs++;
    } else {
        //on alloue une nouvelle zone de mémoire de capacité double
        int * temp_donnees = (int *) malloc(sizeof(int)*tableau->capacite*2);
        //on recopie les anciennes valeurs
        for(int i=0; i < tableau->nbValeurs; i++){
            temp_donnees[i] = tableau->donnees[i];
        }
        //on place la nouvelle valeur
        temp_donnees[tableau->nbValeurs]=valeur;
        tableau->nbValeurs++;
        //on libère l'ancienne zone mémoire
        free(tableau->donnees);
        //on retient la nouvelle zone
        tableau->donnees = temp_donnees;

        /*DynaTableau * newtableau= new DynaTableau;
        initialise(newtableau, (tableau->capacite)*2);
        for(int i=0; i < tableau->nbValeurs; i++){
            newtableau->donnees[i] = tableau->donnees[i];
            newtableau->nbValeurs++;
        }
        newtableau->donnees[newtableau->nbValeurs]=valeur;
        */
    }

}

bool est_vide(const DynaTableau* liste)
{
    if (liste->nbValeurs==0){
        return true;
    }
    return false;
}

void affiche(const DynaTableau* tableau)
{
    for (int i=0; i< tableau->nbValeurs; i++){
        std::cout << "Donnees n° " << i << " : " << tableau->donnees[i] << std::endl;
    }
}

int recupere(const DynaTableau* tableau, int n)
{
    if (!est_vide(tableau)){
        return tableau->donnees[n];
    }
    return 0;
}

int cherche(const DynaTableau* tableau, int valeur)
{
    if (!est_vide(tableau)){
        for (int i=0; i< tableau->nbValeurs; i++){
            if (tableau->donnees[i]==valeur){
                return i;
            }
        }
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    if (!est_vide(tableau)){
        tableau->donnees[n]=valeur;
    } else {
        tableau->donnees[0]=valeur;
        tableau->nbValeurs++;
    }
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    Noeud *nv_noeud = new Noeud;
    if (liste == nullptr){
       std::cout<<"aiie la file n'existe pas"<<std::endl;
    }
    nv_noeud->donnee = valeur;
    nv_noeud->suivant = nullptr;

    if (!est_vide(liste)){
       Noeud *noeud_actuel = liste->premier;
       while (noeud_actuel->suivant != nullptr){
           noeud_actuel = noeud_actuel->suivant;
       }
       noeud_actuel->suivant = nv_noeud;
    }else {
       liste->premier = nv_noeud;
    }
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    if (liste == nullptr){
        std::cout<<"aiie la file n'existe pas, impossible de retirer l'element"<<std::endl;
    }

    int val_defile = 0;

    if (!est_vide((liste))){
        Noeud *noeud_defile = liste->premier;
        val_defile = noeud_defile->donnee;
        liste->premier = noeud_defile->suivant;
        free(noeud_defile);
    }
    return val_defile;
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    Noeud *nv_noeud = new Noeud;
    if (liste==nullptr){
        std::cout<<"aiie la pile n'existe pas"<<std::endl;
    }
    nv_noeud->donnee = valeur;
    nv_noeud->suivant = liste->premier;
    liste->premier = nv_noeud;
}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{
    if (liste==nullptr){
        std::cout<<"aiie la pile n'existe pas, impossible de retirer l'element"<<std::endl;
    }
    int val_retire = 0;
    if (!est_vide(liste))
    {
        Noeud *noeud_depile = liste->premier;
        val_retire = noeud_depile->donnee;
        liste->premier = noeud_depile->suivant;
        free(noeud_depile);
    }
    return val_retire;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
